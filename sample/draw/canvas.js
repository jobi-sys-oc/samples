$(function() {
    var offset = 2;
    var startX;
    var startY;
    var endX;
    var endY;
    var paint = false;

    //2dコンテクストを取得
    var context = $("#canvas")[0].getContext('2d');
    //マウスイベント   mousedown
    $('#canvas').bind('mousedown', function(e) {
        paint = true;
        startX = e.pageX - offset;
        startY = e.pageY - offset;
        return false;
    });

    //マウスイベント - mouseup
    $('#canvas').bind('mouseup', function(e) {
        paint = false;
    });

    //マウスイベント - mouseleave
    $('#canvas').bind('mouseleave', function(e) {
        paint = false;
    });

    //マウスイベント - mousemove
    $('#canvas').bind('mousemove', function(e) {
        if (paint) {
        endX = e.pageX - offset;
        endY = e.pageY - offset;
    
        context.beginPath();
        context.moveTo(startX, startY);
        context.lineTo(endX, endY);
        context.lineCap = "round";
        context.stroke();
        startX = endX;
        startY = endY;
        }
    });

    // *** スマートフォン用 *******************/
    // タッチイベント - touchstart
    $('#canvas').bind('touchstart', function(e) {
        paint = true;
        e.preventDefault();
        startX = e.originalEvent.changedTouches[0].pageX -offset;
        startY = e.originalEvent.changedTouches[0].pageY -offset;
    });

    // *** スマートフォン用 *******************/
    // タッチイベント - touchmove
    $('#canvas').bind('touchmove', function(e) {
        e.preventDefault();
        if (paint) {
            endX = e.originalEvent.changedTouches[0].pageX - offset;
            endY = e.originalEvent.changedTouches[0].pageY - offset;
            context.beginPath();
            context.moveTo(startX, startY);
            context.lineTo(endX, endY);
            context.lineCap = "round";
            context.stroke();
            startX = endX;
            startY = endY;
        }
    });

    // *** スマートフォン用 *******************/
    // タッチイベント   touchend
    $('#canvas').bind('touchend', function(e) {
        paint = false;
    });

    // クリア
    $('#clear').click(function(e) {
        e.preventDefault();
        context.clearRect(0, 0, $('canvas').width(), $('canvas').height());
    });

    // 色選択
    $('td').click(function(e) {
        context.strokeStyle = $(this).css('background-color');
    });

    // サイズ選択
    $('#size').change(function(e) {
        context.lineWidth = $(this).val();
    });
});  